package com.example.javamaverapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaMaverApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaMaverApplication.class, args);
	}

}
