FROM openjdk:11-jre-apline

EXPOSE 8080

COPY ./target/java-maver-application 0.0.1-SNAPSHOT.jar /usr/app/

WORKDIR /usr/app

ENTRYPOINT ["java", "-jar", "java-maver-application 0.0.1-SNAPSHOT.jar"]